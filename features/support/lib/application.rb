class Application

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"
# require_relative '../../support/API-tools'
  require_relative '../../support/hmac-tools'
  require 'date'


#----- Declaring Global variables ----------
  @reference_num = '0'
  @@content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''

  @domain=''
  @basepath=''
  @clientId=''
  @clientSecret=''
  @merchantId=''
  @merchantKey=''

# ------------ constructor,Reading data from yaml file  ----------
  def initialize()
    @hmac = HmacTools.new()

    @header_map={}

    config = YAML.load_file('config.yaml')
    @auth = config['authorization']
    @base_url=config['domain']
    @clientId=config['clientId']
    @contenttype=config['content-type']


    set_url @base_url
  end

  def get_headers()

    @header_map['authorization'] = @auth
    @header_map['domain'] = @base_url
    @header_map['clientId'] = @clientId
    @header_map['content-type'] = @contenttype

  end

# -------- Set Authorization for json request  ----------
  def set_authorization(auth)
    @authorization =auth
  end

# -------- Set URL for json request  ----------
  def set_url(url)
    @url = url
  end

# -------- Post transaction using Reference number  ----------
  def post_body(body)

    full_url= @url+'/applications'

    #Debug urls
    puts "@domain#{@domain}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@clientSecret #{@clientId}"
    puts "@@merchantId #{@authorization}"

    @json_body = body

    #Do post action and get response
    begin

      @response = RestClient.post full_url, @json_body, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end

  def create_bankmerchant()

    full_url= 'https://api-qa.sagepayments.com/merchant/v1/merchants'


    #  @header_map={}

    @json_body = '{ cardData: { number: "5454545454545454", expiration: "1219" } }'

    @header_map['clientId'] = "YF4GPWGEfYZmPtb5RWbZAXNGRV3M3G1T"
    @header_map['clientSecret']  = "rgpGKka7GTlKixyS"
    @header_map['merchantId']  = "999999999997"
    @header_map['merchantKey']  = "K3QD6YWYHFD"
    @header_map['contenttype']  = "application/json"
    @nonce = DateTime.now.strftime('%Q')
    @header_map['nonce'] = @nonce
    @timestamp = DateTime.now.strftime('%Q').to_i/1000
    @header_map['timestamp'] = @timestamp

    #Debug urls
    puts "@domain#{@domain}"
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@clientSecret #{@clientId}"
    puts "@@merchantId #{@merchantId}"

    @authorization = @hmac.hmac(@header_map['clientSecret'], 'POST', full_url, @json_body, '', @header_map['nonce'], @header_map['timestamp'])

    @header_map['Authorization'] = @authorization

    puts "@authorization #{@authorization}"

    puts "@header_map #{@header_map}"

    #Do post action and get response
    begin
        @response = RestClient.post full_url, @json_body, @header_map
        @token_num = JSON.parse(@response.body)['vaultResponse']['data']
        puts "@token_num: #{@token_num}"

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response: #{@response}"

  end


# -------- Set Json Body  ----------
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

# -------- Post HMAC  ----------
  def postHmac(path)
    post_ref path, ''
  end

# -------- Post existing Reference number  ----------
  def post_existing_refnum(path)
    post_ref path, @reference_num
  end

  def get_templatesdata(path)

    full_url= @url+path

    @header_map['authorization'] = @auth
    @header_map['domain'] = @base_url
    @header_map['clientId'] = @clientId
    @header_map['content-type'] = @contenttype

#Do post action and get response
    begin

    @response = RestClient.get full_url, @header_map

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    puts "@response :#{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

  end

# ----------- Verify parameter value into the response body ------------------

  def verify_response_params_contain(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"
    if @parsed_response[response_param].nil?
      expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

# ----------- Verify Token number into the response body ------------------

  def verify_response_for_tokennumber()
    if !@response.nil?
      expect(@response.include? @token_num).to be_truthy, "Expected : #{@token_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@token_num} ,got : Empty response}"
    end
  end

# ----------- Verify response value into the response body ------------------

  def verify_response_value_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_truthy, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{response_value} ,got : Empty response}"
    end
  end

# ----------- Verify response value using reqular expressions into the response body ------------------

  def verify_regex(val, exp)
    if (exp.include? '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
      regex = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/
    else
      if (exp.include? '\d{1,2}')
        regex = /^\d{1,3}$/
      end
    end
    puts "val#{val}"
    resp_val =@parsed_response[val]
    puts "resp_val#{resp_val}"

    status = resp_val.match?(regex)
    puts "status #{status}"

    expect(status).to be_truthy, "#{val} validation failed"

  end

# ----------- Verify Reference number into the response body ------------------

  def verify_response_contain_refnumber()
    puts "@reference_num #{@reference_num}"
    if !@response.nil?
      expect(@response.include? @reference_num).to be_truthy, "Expected : #{@reference_num} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@reference_num} ,got : Empty response}"
    end
  end

# ----------- Verify Order number into the response body ------------------

  def verify_response_contain_ordernumber()
    puts "@reference_num #{@orderNumber}"
    if !@response.nil?
      expect(@response.include? @orderNumber).to be_truthy, "Expected : #{@orderNumber} ,got : #{@response} "
    else
      expect(false).to be_truthy, "Expected : #{@orderNumber} ,got : Empty response}"
    end
  end

# ----------- Verify response body should not contain othe info ------------------

  def verify_response_value_not_contain(response_value)
    puts "response_value:#{response_value}"
    if !@response.nil?
      expect(@response.include? response_value).to be_falsey, "Expected : #{response_value} ,got : #{@response} "
    else
      expect(true).to be_falsey, "Expected : #{response_value} ,got : Empty response}"
    end
  end

# ----------- Verify response body Empty ------------------
  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

# ----------- Verify response Code into response body ------------------

  def verify_response_code resp_code
    expect(@response.code.to_s).to eql(resp_code), "Expected : #{resp_code} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
  end

# ----------- Verify response parameters into response body ------------------

  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if response_param.include? "."
      response_param1 = response_param.split('.').first
      response_param2 = response_param.split('.').last
      #puts "response_param1:#{response_param1} ,response_param2#{response_param2}"
      expected_val =@parsed_response[response_param1][response_param2]
      if expected_val.nil?
        expect(expected_val).to eql(value), "Expected : #{value} ,got : #{expected_val} \nResponesbody:#{@parsed_response}\n"
      else
        # puts expected_val.to_s.strip.length
        expect(expected_val.to_s.strip).to eql(value), "Expected : #{value} ,got : #{expected_val.to_s.strip} \nResponesbody:#{@parsed_response}\n"
      end
    else

      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"

      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end

    end
  end

# ----------- Verify response headers into response body ------------------

  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end

end
