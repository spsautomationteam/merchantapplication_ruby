
# Set required Headers for Json request
And(/^I set (.*) header to (.*)$/) do |header, value|
  @ach.set_header header, value
end

# Set Json Body for request
And(/^I set body to (.*)$/) do |json|
  @ach.set_json_body json
end

# Create HMAC and Send POST request
When(/^I create a bankcard merchant$/) do
  @ach.create_bankmerchant
end

# Create HMAC and Send POST request
When(/^I use HMAC and POST to (.*)$/) do |path|
  @ach.post_tokenbody path
end

# Verify required 'response Headers' at API response
And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @ach.verify_response_headers header_param, val
end

# Verify required Parameters at response Body
Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @ach.verify_response_params param, val
end

# Verify required Parameters at response Body by applying regular expressions
Then(/^response body path (.*) must with regex (.*)$/) do |param, val|
  @ach.verify_regex param, val
end

# Verify required Parameters at response Body using contain method
Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @ach.verify_response_params_contain param, val
end

# Verify required Parameters at response Body using contain method
Then(/^response body path should contain (.*)$/) do | val |
  @ach.verify_response_value_contain  val
end

# Verify response body should not contain required parameters
Then(/^response body path should not contain (.*)$/) do |val|
  @ach.verify_response_value_not_contain val
end

# Create HMAC and GET Sage details
Then(/^I get data for Templates (.*)$/) do | path |
  @ach.get_templatesdata path
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I have valid clientId and Authorization data$/) do
  @ach.get_headers()
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I Submit Merchant Application data with Valid Json Body using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I Submit Merchant Application Without Owners Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I Submit Merchant Application Without PhoneNumber Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Create HMAC then GET List of Existing Current Settled Batches Transactions using parameters value
Then(/^I Submit Merchant Application Without Fees Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without AcceptedCards Details using Body
Then(/^I Submit Merchant Application Without AcceptedCards Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without VisaCard Details using Body
Then(/^I Submit Merchant Application Without VisaCard Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without VisaCard Details using Body
Then(/^I Submit Merchant Application Without MasterCard Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without DiscoverCard Details using Body
Then(/^I Submit Merchant Application Without DiscoverCard Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without AmexCard Details using Body
Then(/^I Submit Merchant Application Without AmexCard Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without Items Details using Body
Then(/^I Submit Merchant Application Without Items Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without Belling Details using Body
Then(/^I Submit Merchant Application Without Belling Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without Address Details using Body
Then(/^I Submit Merchant Application Without Address Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without Contact Details using Body
Then(/^I Submit Merchant Application Without Contact Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Submit Merchant Application Without DateOfBirth Details using Body
Then(/^I Submit Merchant Application Without DateOfBirth Details using Body (.*)$/) do | body|
  @ach.post_body(body)
end

# Verify response Code
Then(/^response code should be (.*)$/) do |responsecode|
  @ach.verify_response_code responsecode
end



